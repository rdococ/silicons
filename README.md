Silicons by rdococ!

Silicons
--------
Welcome to my mod. I guess. They're red and blue striped, they're conductive, and they'll add transistors, resistors and doped silicon to your world.

What?
-----
This mod is an unrealistic electronic circuit simulation including resistors, transistors, a true emitter (power) and a false emitter (ground).
Btw, I don't claim for these to be realistic, they probably aren't. But they're fun to play with nevertheless.

Transistors
-----------
There are two kinds of transistor: the N-type or NPN transistor acts like a switch, allowing current to flow through when the switch is receiving a true signal (e.g. from power), and the P-type or PNP transistor is the opposite, allowing current to flow through when the switch is receiving a false signal. You can craft each with a row of doped silicon ingots corresponding to their names (NPN and PNP). Note that N-type and NPN transistors are different things, similarly to P-type and PNP, but this mod was not designed to be too realistic, neither was its creator well informed on how transistors work.

Resistors
---------
As far as this mod goes, the resistor simply allows a current to pass through if the receiving end doesn't have a true or false signal (a floating signal).

Power & Ground
--------------
The power node supplies a true signal, while the ground node supplies a false signal.

Switches
--------
Switches, like real life circuitry switches, can be used to break an electrical "circuit". Punch it without digging it to change its state from connected, where it will act like a wire, and disconnected, where it will ignore the inputs on either side.
