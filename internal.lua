silicons = {}

silicons.isConductive = function(pos)
	local nodedef = minetest.registered_nodes[minetest.get_node(pos).name]
	if not nodedef then return false end
	if nodedef.groups.conductive then
		return true
	end
end
silicons.isComponent = function(pos)
	local nodedef = minetest.registered_nodes[minetest.get_node(pos).name]
	if not nodedef then return false end
	if nodedef.groups.component then
		return true
	end
end

-- P = VI. R = V/I.
-- thus, V = sqrt(PR = V^2 I/I)?
-- and I = sqrt(P/R).

silicons.getVoltageIR = function (current, resistance)
	return current * resistance
end

silicons.getCurrentVR = function (voltage, resistance)
	return voltage / resistance
end

silicons.getResistanceVI = function (voltage, current)
	return voltage / current
end

silicons.getVoltagePR = function (power, resistance)
	return math.sqrt(power * resistance)
end

silicons.getCurrentPR = function (power, resistance)
	return math.sqrt(power / resistance)
end

silicons.step_check = function(pos, list)
	local values = {}
	for elevation=-1,1 do
		for i=0,3 do
			local offset = vector.add(minetest.facedir_to_dir(i), {x=0,y=elevation,z=0})
			local step = vector.add(pos, offset)
			if not list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] then
				if silicons.isConductive(step) then
					list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] = true
					for _,j in pairs(silicons.step_check(step, list)) do
						table.insert(values, j)
					end
				elseif elevation == 0 and silicons.isComponent(step) and minetest.get_meta(step):get_string("output" .. tostring(i)) then
					list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] = true
					table.insert(values, minetest.get_meta(step):get_string("output" .. tostring(((i + 2) % 4))) )
				end
			end
		end
	end
	return values
end
silicons.get_boolean_value = function(inputTable)
	local inputA = "floating"
	if inputTable then
		for _,i in pairs(inputTable) do
			if i == "false" then inputA = "false" end
			if i == "true" then inputA = "true" end
		end
	end
	return inputA
end

-- Registering a Component:
-- The function is run with the parameters (pos, neigh).
-- Pos is the position of the component, and Neigh is a list of inputs the component is receiving.
-- The function is expected to use metadata to set the outputs of the component.
-- "output" .. facedir is for the output of the component in that facedir.
-- The function is also expected to tell the mod what inputs it interacts with through metadata (to assist with the wire connection system).
-- "input" .. facedir is for the input of the component in that facedir.

silicons.register_component = function(name, func)
	minetest.register_abm{
		nodenames = {name},
		neighbors = {},
		interval = 1,
		chance = 1,
		action = function(pos)
			local neigh = {}
			for i=0,3 do
				local offset = minetest.facedir_to_dir(i)
				local step = vector.add(pos, offset)
				
				local nodedef = minetest.registered_nodes[minetest.get_node(step).name]
				if not nodedef then nodedef = {groups = {}} end
				
				if nodedef.groups.conductive or nodedef.groups.component then
					if nodedef.groups.component then
						neigh[i] = {minetest.get_meta(step):get_string("output" .. tostring(((i + 2) % 4)))}
					else
						neigh[i] = silicons.step_check(step, {[tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z)] = true})
					end
				end
			end
			minetest.get_meta(pos):set_string("infotext", minetest.serialize(neigh))
			func(pos, neigh)
		end,
	}
end
