for f0=0,1 do
	for f1=0,1 do
		for f2=0,1 do
			for f3=0,1 do
				local nodebox = {}
				
				local thickness = 0.1
				local joint = 0.15
				local length = 0.6
				
				local override = f0 + f1 + f2 + f3 < 1
				
				if f0 > 0 or override then -- +z
					table.insert(nodebox, {-thickness, -0.5, 0.5-length, thickness, thickness-0.5, 0.5})
				end
				if f1 > 0 or override then -- +x
					table.insert(nodebox, {0.5-length, -0.5, -thickness, 0.5, thickness-0.5, thickness})
				end
				if f2 > 0 or override then -- -z
					table.insert(nodebox, {-thickness, -0.5, -0.5, thickness, thickness-0.5, length-0.5})
				end
				if f3 > 0 or override then -- -x
					table.insert(nodebox, {-0.5, -0.5, -thickness, length-0.5, thickness-0.5, thickness})
				end
				
				if f0 + f1 + f2 + f3 > 2 or override then
					table.insert(nodebox, {-joint, -0.5, -joint, joint, joint-0.5, joint})
				end
				
				groups = {oddly_breakable_by_hand = 3, conductive = 1}
				if f0 < 1 or f1 < 1 or f2 < 1 or f3 < 1 then
					groups["not_in_creative_inventory"] = 1
				end
				
				minetest.register_node("silicons:wire" .. tostring(f0) .. tostring(f1) .. tostring(f2) .. tostring(f3), {
					description = "Wire",
					drawtype = "nodebox",
					node_box = {
						type = "fixed",
						fixed = --[[{
							{-0.5, -0.5, -0.5, 0.5, -0.49, 0.5}
						}]]nodebox
					},
					selection_box = {
						type = "fixed",
						fixed = {-0.5, -0.5, -0.5, 0.5, thickness-0.5, 0.5}
					},
					
					paramtype = "light",

					tiles = {"silicons_wire.png"},
					groups = groups,
					drop = "silicons:wire1111"
				})
				minetest.register_abm({
					nodenames = {"silicons:wire" .. tostring(f0) .. tostring(f1) .. tostring(f2) .. tostring(f3)},
					neighbors = {},
					interval = 1,
					chance = 1,
					action = function(pos)
						local f = {[0] = "0", [1] = "0", [2] = "0", [3] = "0"}
						for elevation=-1,1 do
							for i=0,3 do
								local neigh = vector.add(pos, vector.add(minetest.facedir_to_dir(i), {x=0,y=elevation,z=0}) )
								if silicons.isConductive(neigh) or (silicons.isComponent(neigh) and elevation == 0 and
									(minetest.get_meta(neigh):get_string("output" .. tostring(i + 2) % 4) ~= "" or minetest.get_meta(neigh):get_string("input" .. tostring(i + 2) % 4) ~= "")) then
									f[i] = "1"
								end
							end
						end
						
						minetest.set_node(pos, {name = "silicons:wire" .. tostring(f[0]) .. tostring(f[1]) .. tostring(f[2]) .. tostring(f[3])})
					end
				})
			end
		end
	end
end
