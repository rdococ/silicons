minetest.register_node("silicons:lamp_off", {
	description = "Lamp",
	tiles = {"silicons_lamp_off.png"},
	groups = {oddly_breakable_by_hand = 3, conductive = 1}
})
silicons.register_component("silicons:lamp_off", function (pos, inputs)
	for _,i in pairs(inputs) do
		if silicons.get_boolean_value(i) == "true" then minetest.set_node(pos, {name = "silicons:lamp_on"}); return end
	end
end)

minetest.register_node("silicons:lamp_on", {
	description = "Active Lamp (you hacker!)",
	tiles = {"silicons_lamp_on.png"},
	groups = {oddly_breakable_by_hand = 3, conductive = 1, not_in_creative_inventory = 1},
	light_source = 15,
	drop = "silicons:lamp_off"
})
silicons.register_component("silicons:lamp_on", function (pos, inputs)
	for _,i in pairs(inputs) do
		if silicons.get_boolean_value(i) == "true" then return end
	end
	minetest.set_node(pos, {name = "silicons:lamp_off"})
end)

minetest.register_node("silicons:not_gate", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Not Gate",
	tiles = {"silicons_not_gate.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:not_gate", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	local output = "true"
	if silicons.get_boolean_value(inputs[((facedir + 2) % 4)]) == "true" then
		output = "false"
	elseif silicons.get_boolean_value(inputs[((facedir + 2) % 4)]) == "floating" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
end)

minetest.register_node("silicons:p_transistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "P-Type Transistor",
	tiles = {"silicons_p_transistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:p_transistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local inputA = silicons.get_boolean_value(inputs[((facedir + 2) % 4)])
	output = inputA

	local inputB = silicons.get_boolean_value(inputs[((facedir - 1) % 4)])
	
	if inputB ~= "false" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. ((facedir - 1) % 4), "listening")
end)

minetest.register_node("silicons:n_transistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "N-Type Transistor",
	tiles = {"silicons_n_transistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:n_transistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local inputA = silicons.get_boolean_value(inputs[((facedir + 2) % 4)])
	output = inputA

	local inputB = silicons.get_boolean_value(inputs[((facedir - 1) % 4)])
	
	if inputB ~= "true" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta("output" .. facedir, output)
	
	meta("input" .. ((facedir + 2) % 4), "listening")
	meta("input" .. ((facedir - 1) % 4), "listening")
end)

minetest.register_node("silicons:up_resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "Pull-Up Resistor",
	tiles = {"silicons_up_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:up_resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local output = silicons.get_boolean_value(inputs[facedir])
	if output == "floating" then output = "true" end
	
	minetest.get_meta(pos):set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	minetest.get_meta(pos):set_string("output" .. facedir, output)
end)

minetest.register_node("silicons:down_resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Pull-Down Resistor",
	tiles = {"silicons_down_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:down_resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local output = silicons.get_boolean_value(inputs[facedir])
	if output == "floating" then output = "false" end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
end)

minetest.register_node("silicons:resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Resistor",
	tiles = {"silicons_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local outputA = silicons.get_boolean_value(inputs[facedir])
	if outputA == "floating" then outputA = silicons.get_boolean_value(inputs[((facedir + 2) % 4)]) else outputA = "" end
	
	local outputB = silicons.get_boolean_value(inputs[((facedir + 2) % 4)])
	if outputB == "floating" then outputB = silicons.get_boolean_value(inputs[facedir]) else outputB = "" end

	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutputs: " .. outputA .. "\n" .. outputB .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, outputA)
	meta:set_string("output" .. ((facedir + 2) % 4), outputB)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:switch_connected", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Connected Switch",
	tiles = {"silicons_switch_connected.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1, not_in_creative_inventory = 1},
	paramtype2 = "facedir",
	
	drop = "silicons:switch_disconnected",
	
	on_punch = function (pos, node)
		minetest.set_node(pos, {name="silicons:switch_disconnected", param=node.param, param2=node.param2})
	end
})
silicons.register_component("silicons:switch_connected", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local outputA = silicons.get_boolean_value(inputs[((facedir + 2) % 4)])
	
	local outputB = silicons.get_boolean_value(inputs[facedir])
	
	local meta = minetest.get_meta(pos)

	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutputs: " .. outputA .. "\n" .. outputB .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, outputA)
	meta:set_string("output" .. ((facedir + 2) % 4), outputB)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:switch_disconnected", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Switch",
	tiles = {"silicons_switch_disconnected.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir",
	
	on_punch = function (pos, node)
		minetest.set_node(pos, {name="silicons:switch_connected", param=node.param, param2=node.param2})
	end
})
silicons.register_component("silicons:switch_disconnected", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:power", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Power",
	tiles = {"silicons_power.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:power", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. "true" .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, "true")
end)

minetest.register_node("silicons:ground", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Ground",
	tiles = {"silicons_ground.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:ground", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. "false" .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, "false")
end)

minetest.register_node("silicons:diode", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Diode",
	tiles = {"silicons_diode.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
silicons.register_component("silicons:diode", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	local output = silicons.get_boolean_value(inputs[((facedir + 2) % 4)])
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
end)

