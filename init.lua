local modpath = minetest.get_modpath("silicons")

-- Internals
dofile(modpath.."/internal.lua")

-- Components
dofile(modpath.."/components.lua")

-- Wires
dofile(modpath.."/wire.lua")

-- Recipes
dofile(modpath.."/crafting.lua")

--[[local glooptest_installed = minetest.get_modpath("glooptest")
local technic_installed = minetest.get_modpath("technic")
local quartz_installed = minetest.get_modpath("quartz")

local function isConductive(pos)
	local nodedef = minetest.registered_nodes[minetest.get_node(pos).name]
	if not nodedef then return false end
	if nodedef.groups.conductive then
		return true
	end
end
local function isComponent(pos)
	local nodedef = minetest.registered_nodes[minetest.get_node(pos).name]
	if not nodedef then return false end
	if nodedef.groups.component then
		return true
	end
end

local function step_check(pos, list)
	local values = {}
	for elevation=-1,1 do
		for i=0,3 do
			local offset = vector.add(minetest.facedir_to_dir(i), {x=0,y=elevation,z=0})
			local step = vector.add(pos, offset)
			if not list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] then
				if isConductive(step) then
					list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] = true
					for _,j in pairs(step_check(step, list)) do
						table.insert(values, j)
					end
				elseif elevation == 0 and isComponent(step) and minetest.get_meta(step):get_string("output" .. tostring(i)) then
					list[tostring(step.x) .. "," .. tostring(step.y) .. "," .. tostring(step.z)] = true
					table.insert(values, minetest.get_meta(step):get_string("output" .. tostring(((i + 2) % 4))) )
				end
			end
		end
	end
	return values
end
function get_boolean_value(inputTable)
	local inputA = "floating"
	if inputTable then
		for _,i in pairs(inputTable) do
			if i == "false" then inputA = "false" end
			if i == "true" then inputA = "true" end
		end
	end
	return inputA
end

-- Registering a Component:
-- The function is run with the parameters (pos, neigh).
-- Pos is the position of the component, and Neigh is a list of inputs the component is receiving.
-- The function is expected to use metadata to set the outputs of the component.
-- "output" .. facedir is for the output of the component in that facedir.
-- The function is also expected to set the inputs the component listens to (to a non-empty string).
-- "input" .. facedir is for the input of the component in that facedir.

function register_component(name, func)
	minetest.register_abm{
		nodenames = {name},
		neighbors = {},
		interval = 1,
		chance = 1,
		action = function(pos)
			local neigh = {}
			for i=0,3 do
				local offset = minetest.facedir_to_dir(i)
				local step = vector.add(pos, offset)
				
				local nodedef = minetest.registered_nodes[minetest.get_node(step).name]
				if not nodedef then nodedef = {groups = {}} end
				
				if nodedef.groups.conductive or nodedef.groups.component then
					if nodedef.groups.component then
						neigh[i] = {minetest.get_meta(step):get_string("output" .. tostring(((i + 2) % 4)))}
					else
						neigh[i] = step_check(step, {[tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z)] = true})
					end
				end
			end
			minetest.get_meta(pos):set_string("infotext", minetest.serialize(neigh))
			func(pos, neigh)
		end,
	}
end

-- silicon_wire_piece.png
for f0=0,1 do
	for f1=0,1 do
		for f2=0,1 do
			for f3=0,1 do
				local nodebox = {}
				
				local thickness = 0.1
				local joint = 0.15
				local length = 0.6
				
				local override = f0 + f1 + f2 + f3 < 1
				
				if f0 > 0 or override then -- +z
					--texture = texture .. "(silicon_wire_piece.png^[transformR180])^"
					table.insert(nodebox, {-thickness, -0.5, 0.5-length, thickness, thickness-0.5, 0.5})
				end
				if f1 > 0 or override then -- +x
					--texture = texture .. "(silicon_wire_piece.png^[transformR90])^"
					table.insert(nodebox, {0.5-length, -0.5, -thickness, 0.5, thickness-0.5, thickness})
				end
				if f2 > 0 or override then -- -z
					--texture = texture .. "(silicon_wire_piece.png)^"
					table.insert(nodebox, {-thickness, -0.5, -0.5, thickness, thickness-0.5, length-0.5})
				end
				if f3 > 0 or override then -- -x
					--texture = texture .. "(silicon_wire_piece.png^[transformR270])^"
					table.insert(nodebox, {-0.5, -0.5, -thickness, length-0.5, thickness-0.5, thickness})
				end
				
				-- ((f0 > 0 and f1 > 0) or (f1 > 0 and f2 > 0) or (f2 > 0 and f3 > 0) or (f3 > 0 and f0 > 0))
				if f0 + f1 + f2 + f3 > 2 or override then
					table.insert(nodebox, {-joint, -0.5, -joint, joint, joint-0.5, joint})
				end
				
				groups = {oddly_breakable_by_hand = 3, conductive = 1}
				if f0 < 1 or f1 < 1 or f2 < 1 or f3 < 1 then
					groups["not_in_creative_inventory"] = 1
				end
				
				minetest.register_node("silicons:wire" .. tostring(f0) .. tostring(f1) .. tostring(f2) .. tostring(f3), {
					description = "Wire",
					drawtype = "nodebox",
					node_box = {
						type = "fixed",
						fixed = --[[{
							{-0.5, -0.5, -0.5, 0.5, -0.49, 0.5}
						}]=]nodebox
					},
					selection_box = {
						type = "fixed",
						fixed = {-0.5, -0.5, -0.5, 0.5, thickness-0.5, 0.5}
					},
					
					paramtype = "light",

					tiles = {"silicons_wire.png"},
					groups = groups,
					drop = "silicons:wire1111"
				})
				minetest.register_abm({
					nodenames = {"silicons:wire" .. tostring(f0) .. tostring(f1) .. tostring(f2) .. tostring(f3)},
					neighbors = {},
					interval = 1,
					chance = 1,
					action = function(pos)
						local f = {[0] = "0", [1] = "0", [2] = "0", [3] = "0"}
						for elevation=-1,1 do
							for i=0,3 do
								local neigh = vector.add(pos, vector.add(minetest.facedir_to_dir(i), {x=0,y=elevation,z=0}) )
								if isConductive(neigh) or (isComponent(neigh) and elevation == 0 and
									(minetest.get_meta(neigh):get_string("output" .. tostring(i + 2) % 4) ~= "" or minetest.get_meta(neigh):get_string("input" .. tostring(i + 2) % 4) ~= "")) then
									f[i] = "1"
								end
							end
						end
						
						minetest.set_node(pos, {name = "silicons:wire" .. tostring(f[0]) .. tostring(f[1]) .. tostring(f[2]) .. tostring(f[3])})
					end
				})
			end
		end
	end
end

minetest.register_node("silicons:lamp_off", {
	description = "Lamp",
	tiles = {"silicons_lamp_off.png"},
	groups = {oddly_breakable_by_hand = 3, conductive = 1}
})
register_component("silicons:lamp_off", function (pos, inputs)
	for _,i in pairs(inputs) do
		if get_boolean_value(i) == "true" then minetest.set_node(pos, {name = "silicons:lamp_on"}); return end
	end
end)

minetest.register_node("silicons:lamp_on", {
	description = "Active Lamp (you hacker!)",
	tiles = {"silicons_lamp_on.png"},
	groups = {oddly_breakable_by_hand = 3, conductive = 1, not_in_creative_inventory = 1},
	light_source = 15,
	drop = "silicons:lamp_off"
})
register_component("silicons:lamp_on", function (pos, inputs)
	for _,i in pairs(inputs) do
		if get_boolean_value(i) == "true" then return end
	end
	minetest.set_node(pos, {name = "silicons:lamp_off"})
end)

minetest.register_node("silicons:not_gate", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Not Gate",
	tiles = {"silicons_not_gate.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:not_gate", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	local output = "true"
	if get_boolean_value(inputs[((facedir + 2) % 4)]) == "true" then
		output = "false"
	elseif get_boolean_value(inputs[((facedir + 2) % 4)]) == "floating" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
end)

minetest.register_node("silicons:p_transistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "P-Type Transistor",
	tiles = {"silicons_p_transistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:p_transistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local inputA = get_boolean_value(inputs[((facedir + 2) % 4)])
	output = inputA

	local inputB = get_boolean_value(inputs[((facedir - 1) % 4)])
	
	if inputB ~= "false" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. ((facedir - 1) % 4), "listening")
end)

minetest.register_node("silicons:n_transistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "N-Type Transistor",
	tiles = {"silicons_n_transistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:n_transistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local inputA = get_boolean_value(inputs[((facedir + 2) % 4)])
	output = inputA

	local inputB = get_boolean_value(inputs[((facedir - 1) % 4)])
	
	if inputB ~= "true" then
		output = "floating"
	end
	
	local meta = minetest.get_meta(pos)
	meta("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta("output" .. facedir, output)
	
	meta("input" .. ((facedir + 2) % 4), "listening")
	meta("input" .. ((facedir - 1) % 4), "listening")
end)

minetest.register_node("silicons:up_resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",

	description = "Pull-Up Resistor",
	tiles = {"silicons_up_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:up_resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local output = get_boolean_value(inputs[facedir])
	if output == "floating" then output = "true" end
	
	minetest.get_meta(pos):set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	minetest.get_meta(pos):set_string("output" .. facedir, output)
end)

minetest.register_node("silicons:down_resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Pull-Down Resistor",
	tiles = {"silicons_down_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:down_resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2

	local output = get_boolean_value(inputs[facedir])
	if output == "floating" then output = "false" end
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
end)

minetest.register_node("silicons:resistor", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Resistor",
	tiles = {"silicons_resistor.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:resistor", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local outputA = get_boolean_value(inputs[facedir])
	if outputA == "floating" then outputA = get_boolean_value(inputs[((facedir + 2) % 4)]) else outputA = "" end
	
	local outputB = get_boolean_value(inputs[((facedir + 2) % 4)])
	if outputB == "floating" then outputB = get_boolean_value(inputs[facedir]) else outputB = "" end

	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutputs: " .. outputA .. "\n" .. outputB .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, outputA)
	meta:set_string("output" .. ((facedir + 2) % 4), outputB)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:switch_connected", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Connected Switch",
	tiles = {"silicons_switch_connected.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1, not_in_creative_inventory = 1},
	paramtype2 = "facedir",
	
	drop = "silicons:switch_disconnected",
	
	on_punch = function (pos, node)
		minetest.set_node(pos, {name="silicons:switch_disconnected", param=node.param, param2=node.param2})
	end
})
register_component("silicons:switch_connected", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local outputA = get_boolean_value(inputs[((facedir + 2) % 4)])
	
	local outputB = get_boolean_value(inputs[facedir])
	
	local meta = minetest.get_meta(pos)

	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutputs: " .. outputA .. "\n" .. outputB .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, outputA)
	meta:set_string("output" .. ((facedir + 2) % 4), outputB)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:switch_disconnected", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Switch",
	tiles = {"silicons_switch_disconnected.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir",
	
	on_punch = function (pos, node)
		minetest.set_node(pos, {name="silicons:switch_connected", param=node.param, param2=node.param2})
	end
})
register_component("silicons:switch_disconnected", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
	meta:set_string("input" .. facedir, "listening")
end)

minetest.register_node("silicons:power", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Power",
	tiles = {"silicons_power.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:power", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. "true" .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, "true")
end)

minetest.register_node("silicons:ground", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Power",
	tiles = {"silicons_ground.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:ground", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. "false" .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, "false")
end)

minetest.register_node("silicons:diode", {
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {{-0.5,-0.5,-0.5,0.5,-0.4,0.5}}
	},
	paramtype = "light",
	
	description = "Diode",
	tiles = {"silicons_diode.png"},
	groups = {oddly_breakable_by_hand = 3, component = 1},
	paramtype2 = "facedir"
})
register_component("silicons:diode", function (pos, inputs)
	-- Facedir
	local facedir = minetest.get_node(pos).param2
	local output = get_boolean_value(inputs[((facedir + 2) % 4)])
	
	local meta = minetest.get_meta(pos)
	meta:set_string("infotext", "facedir: " .. tostring(facedir) .. "\noutput: " .. output .. "\ninputs: " .. minetest.get_meta(pos):get_string("infotext"))
	meta:set_string("output" .. facedir, output)
	
	meta:set_string("input" .. ((facedir + 2) % 4), "listening")
end)

minetest.register_craftitem("silicons:silica_dust", {
	description = "Silica Dust",
	inventory_image = "silicons_silica_dust.png",
})
minetest.register_craftitem("silicons:silicon_ingot", {
	description = "Silicon Ingot",
	inventory_image = "silicons_silicon_ingot.png",
})

minetest.register_craftitem("silicons:silicon_ingot_p", {
	description = "P-type Silicon Ingot",
	inventory_image = "silicons_silicon_ingot_p.png",
})
minetest.register_craftitem("silicons:silicon_ingot_n", {
	description = "N-type Silicon Ingot",
	inventory_image = "silicons_silicon_ingot_n.png",
})

if technic_installed and glooptest_installed then
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:alatro_ingot"}, output = "silicons:silicon_ingot_p"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:akalin_ingot"}, output = "silicons:silicon_ingot_p"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:talinite_ingot"}, output = "silicons:silicon_ingot_n"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:arol_ingot"}, output = "silicons:silicon_ingot_n"})
end

--quartz:quartz_crystal

if quartz_installed then
	minetest.register_craft({
		type = "cooking",
		output = "silicons:silica_dust",
		recipe = "quartz:quartz_crystal",
		cooktime = 3,
	})
end
minetest.register_craft({
    type = "cooking",
    output = "silicons:silicon_ingot",
    recipe = "silicons:silica_dust",
    cooktime = 3,
})

minetest.register_craft({
    output = 'silicons:p_transistor',
    recipe = {
        {'', '', ''},
        {'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n', 'silicons:silicon_ingot_p'},
        {'', '', ''}, -- Also groups; e.g. 'group:crumbly'
    },
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]=]
})
minetest.register_craft({
    output = 'silicons:n_transistor',
    recipe = {
        {'', '', ''},
        {'silicons:silicon_ingot_n', 'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n'},
        {'', '', ''}, -- Also groups; e.g. 'group:crumbly'
    },
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]=]
})
minetest.register_craft({
	type = "shapeless",
    output = 'silicons:diode',
    recipe = {'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n'}
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]=]
})

minetest.register_craft({
    output = 'silicons:resistor',
    recipe = {
		{'default:paper', 'default:paper', 'default:paper'},
	    {'technic:fine_copper_wire', 'default:coal_lump', 'technic:fine_copper_wire'},
	    {'default:paper', 'default:paper', 'default:paper'}, -- Also groups; e.g. 'group:crumbly'
	},
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]=]
})

if technic_installed then
	minetest.register_craft({
		output = 'silicons:wire1111 3',
		recipe = {
		    {'default:paper', 'default:paper', 'default:paper'},
		    {'technic:fine_copper_wire', 'technic:fine_copper_wire', 'technic:fine_copper_wire'},
		    {'default:paper', 'default:paper', 'default:paper'}, -- Also groups; e.g. 'group:crumbly'
		},
		--[[replacements = <optional list of item pairs,
		                replace one input item with another item on crafting>]=]
	})
end]]
