local glooptest_installed = minetest.get_modpath("glooptest")
local technic_installed = minetest.get_modpath("technic")
local quartz_installed = minetest.get_modpath("quartz")

minetest.register_craftitem("silicons:silica_dust", {
	description = "Silica Dust",
	inventory_image = "silicons_silica_dust.png",
})
minetest.register_craftitem("silicons:silicon_ingot", {
	description = "Silicon Ingot",
	inventory_image = "silicons_silicon_ingot.png",
})

minetest.register_craftitem("silicons:silicon_ingot_p", {
	description = "P-type Silicon Ingot",
	inventory_image = "silicons_silicon_ingot_p.png",
})
minetest.register_craftitem("silicons:silicon_ingot_n", {
	description = "N-type Silicon Ingot",
	inventory_image = "silicons_silicon_ingot_n.png",
})

if technic_installed and glooptest_installed then
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:alatro_ingot"}, output = "silicons:silicon_ingot_p"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:akalin_ingot"}, output = "silicons:silicon_ingot_p"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:talinite_ingot"}, output = "silicons:silicon_ingot_n"})
	technic.register_alloy_recipe({input = {"silicons:silicon_ingot", "glooptest:arol_ingot"}, output = "silicons:silicon_ingot_n"})
end

--quartz:quartz_crystal

if quartz_installed then
	minetest.register_craft({
		type = "cooking",
		output = "silicons:silica_dust",
		recipe = "quartz:quartz_crystal",
		cooktime = 3,
	})
end
minetest.register_craft({
    type = "cooking",
    output = "silicons:silicon_ingot",
    recipe = "silicons:silica_dust",
    cooktime = 3,
})

minetest.register_craft({
    output = 'silicons:p_transistor',
    recipe = {
        {'', '', ''},
        {'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n', 'silicons:silicon_ingot_p'},
        {'', '', ''}, -- Also groups; e.g. 'group:crumbly'
    },
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]]
})
minetest.register_craft({
    output = 'silicons:n_transistor',
    recipe = {
        {'', '', ''},
        {'silicons:silicon_ingot_n', 'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n'},
        {'', '', ''}, -- Also groups; e.g. 'group:crumbly'
    },
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]]
})
minetest.register_craft({
	type = "shapeless",
    output = 'silicons:diode',
    recipe = {'silicons:silicon_ingot_p', 'silicons:silicon_ingot_n'}
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]]
})

minetest.register_craft({
    output = 'silicons:resistor',
    recipe = {
		{'default:paper', 'default:paper', 'default:paper'},
	    {'technic:fine_copper_wire', 'default:coal_lump', 'technic:fine_copper_wire'},
	    {'default:paper', 'default:paper', 'default:paper'}, -- Also groups; e.g. 'group:crumbly'
	},
    --[[replacements = <optional list of item pairs,
                    replace one input item with another item on crafting>]]
})

if technic_installed then
	minetest.register_craft({
		output = 'silicons:wire1111 3',
		recipe = {
		    {'default:paper', 'default:paper', 'default:paper'},
		    {'technic:fine_copper_wire', 'technic:fine_copper_wire', 'technic:fine_copper_wire'},
		    {'default:paper', 'default:paper', 'default:paper'}, -- Also groups; e.g. 'group:crumbly'
		},
		--[[replacements = <optional list of item pairs,
		                replace one input item with another item on crafting>]]
	})
end
